// if-else
var nama = "John"
var peran = ""

if (nama == "") {
	console.log("Nama harus diisi!")
}
else {
	if (peran == "") {
		console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!")
	}
	else {
		console.log("Selamat datang di Dunia Werewolf, " + nama)

		if (peran == "penyihir") {
			console.log("Halo " + peran + " " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
		}
		else if (peran == "werewolf") {
			console.log("Halo " + peran + " " + nama + ", Kamu akan memakan mangsa setiap malam!")
		}
		else if (peran == "guard") {
			console.log("Halo " + peran + " " + nama + ", Kamu akan membantu melindungi temanmu dari serangan werewolf.")
		}
		else {
			console.log("Halo " + peran + " " + nama + ", Kamu tidak memiliki tugas.")
		}
	}
}


// switch case
var hari = 21; 
var bulan = 1; 
var tahun = 1945;
var strBulan = ""

switch (bulan){
	case 1:{
		strBulan = "Januari"
		break
	}

	case 2:{
		strBulan = "Februari"
		break
	}

	case 3:{
		strBulan = "Maret"
		break
	}

	case 4:{
		strBulan = "April"
		break
	}

	case 5:{
		strBulan = "Mei"
		break
	}

	case 6:{
		strBulan = "Juni"
		break
	}

	case 7:{
		strBulan = "Juli"
		break
	}

	case 8:{
		strBulan = "Agustus"
		break
	}

	case 9:{
		strBulan = "September"
		break
	}

	case 10:{
		strBulan = "Oktober"
		break
	}

	case 11:{
		strBulan = "November"
		break
	}

	case 12:{
		strBulan = "Desember"
		break
	}

	default : {strBulan = "undefined"}
}

console.log(hari + " " + strBulan + " " + tahun)