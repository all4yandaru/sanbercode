var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

time = 10000
var willReadBooks

willReadBooks = new readBooksPromise(time, books[0])
		.then(function (time) {
            readBooksPromise(time, books[1])
            	.then(function (time) {
		            readBooksPromise(time, books[2])
		            	.then(function (time) {
				            console.log("Selesai baca buku")
				        })
				        .catch(function (time) {
				        	console.log("waktu yang dibutuhkan adalah " + time*-1%999 + " detik")
				        });
		        })
		        .catch(function (time) {
		        	console.log("waktu yang dibutuhkan adalah " + time*-1%999 + " detik")
		        });
        })
        .catch(function (time) {
        	console.log("waktu yang dibutuhkan adalah " + time*-1%999 + " detik")
        });