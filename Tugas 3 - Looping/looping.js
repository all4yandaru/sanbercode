// soal 1
console.log("LOOPING PERTAMA");
var loop = 1
while(loop <= 20){
	if (loop%2 == 0) {
		console.log(loop + " - I love coding");
	}
	loop++;
}

console.log("LOOPING KEDUA");
while(loop > 0){
	if (loop%2 == 0) {
		console.log(loop + " - I will become a mobile developer");
	}
	loop--;
}
console.log("");


// soal 2
for (var i = 1; i <= 20; i++) {
	if (i%2 == 0) { // genap
		console.log(i + " - Berkualitas")
	}
	else { // ganjil
		if (i%3 == 0) { // kelipatan tiga
			console.log(i + " - I Love Coding")
		}
		else {
			console.log(i + " - Santai")
		}
	}
}
console.log();


// soal 3
for (var i = 0; i < 4; i++) {
	for (var j = 0; j < 8; j++) {
		process.stdout.write("# ");
	}
	console.log();
}
console.log();


// soal 4
for (var i = 0; i < 7; i++) {
	for (var j = 0; j <= i; j++) {
		process.stdout.write("# ");
	}
	console.log();
}
console.log();


// soal 5
for (var i = 0; i < 8; i++) {
	for (var j = 0; j < 4; j++) {
		if (i%2 == 0) {
			process.stdout.write(" #");
		}
		else {
			process.stdout.write("# ");
		}
	}
	console.log();
}