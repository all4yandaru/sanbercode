// SOAL 1
console.log("SOAL 1 =====================================================================")
var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

function arrayToObject(arr) {
    // Code di sini 
    var object = [{firstName: "", lastName: "", gender: "", age: 0}]
    var objectTemp
    var ada = (arr[0]!=null)

    if (!ada) { // kalau array kosong
        console.log("Array kosong")
    }
    else { // kalau array ada isinya
        for (var i = 0; i < arr.length; i++) {
            ada = (arr[i][2]!=null)
            // masukin ke variabel temporary dulu
            if (ada & thisYear - arr[i][3] > 0) {
                objectTemp = {
                    firstName : arr[i][0],
                    lastName : arr[i][1],
                    gender : arr[i][2],
                    age : thisYear - arr[i][3]
                }
            }
            else { // kalau tahunnya salah
                objectTemp = {
                    firstName : arr[i][0],
                    lastName : arr[i][1],
                    gender : arr[i][2],
                    age : "Invalid Birth Year"
                }
            }
            if (i==0) {
                object[0] = objectTemp
            }
            else {
                object.push(objectTemp)
            }
        }

        for (var i = 0; i < arr.length; i++) {
            console.log((i+1) + ". " + object[i].firstName + " " + object[i].lastName)
            console.log(object[i])
        }
    }
    console.log()
}

// Driver Code
var people = [ 
    ["Bruce", "Banner", "male", 1975], 
    ["Natasha", "Romanoff", "female"] 
]
arrayToObject(people) 
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)
 
// Error case 
arrayToObject([]) // "Array Kosong"


// SOAL 2
console.log("SOAL 2 =====================================================================")

function shoppingTime(memberId=null, money=null) {
  // you can only write your code here!

  var dataBarang = [
    {nama : "Sepatu Stacattu",harga : 1500000},{nama : "Baju Zoro",harga : 500000},
    {nama : "Baju H&N",harga : 250000},{nama : "Sweater Uniklooh",harga : 175000},
    {nama : "Casing Handphone",harga : 50000}
  ]

  var belanja = {memberId:'', money:'', listPurchased:[]}// kerangka
  var cart = [] // keranjang belanjaan

  if (memberId==null || memberId=='') {
    return("Mohon maaf, toko X hanya berlaku untuk member saja")
  }
  else if (money < 50000 || money==null) {
    return("Mohon maaf, uang tidak cukup")
  }
  else {
    belanja.memberId = memberId
    belanja.money = money
    for (var i = 0; i < dataBarang.length; i++) {
        if (dataBarang[i].harga <= money) {
            cart.push(dataBarang[i].nama)
            money-=dataBarang[i].harga
        }
    }
    belanja.listPurchased = cart
    return belanja
  }
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log()


// SOAL 3
console.log("SOAL 3 =====================================================================")
function naikAngkot(arrPenumpang) {
  var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  //your code here

  var angkot = []
  for (var i = 0; i < arrPenumpang.length; i++) {
      var awal = 0, akhir = 0
      var jalan = false
      for (var j = 0; j < rute.length; j++) {
          if (arrPenumpang[i][1] == rute[j]) {
            awal = j
            jalan = true
          }
          if (jalan && arrPenumpang[i][2] == rute[j]) {
            akhir = j
          }
      }
      var jarak = akhir-awal

      if (jarak < 0 || !jalan) { // jika asal atau tujuan tidak ada dalam rute
        harga = " Asal atau Tujuan tidak ada dalam rute"
      }
      else {
        var harga = jarak*2000
      }

      
      var dataPenumpang = {
        penumpang: arrPenumpang[i][0],
        naikDari: arrPenumpang[i][1],
        tujuan: arrPenumpang[i][2],
        bayar: harga
      }
      angkot.push(dataPenumpang)
  }
  return angkot
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]
console.log()