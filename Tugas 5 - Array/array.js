// soal 1
function range(num1, num2){
	var angka = []
	if (num1==null || num2==null) {
		return -1
	}
	else if (num1 > num2) {
		for (var i = num1; i > num2-1; i--) {
			angka.push(i)
		}
		return angka
	}
	else {
		for (var i = num1; i < num2+1; i++) {
			angka.push(i)
		}
		return angka
	}
}

console.log("SOAL 1")
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log()


// soal 2
function rangeWithStep(num1, num2, step=1){
	var angka = []
	if (num1==null || num2==null) {
		return -1
	}
	else if (num1 > num2) {
		for (var i = num1; i > num2-1; i-=step) {
			angka.push(i)
		}
		return angka
	}
	else {
		for (var i = num1; i < num2+1; i+=step) {
			angka.push(i)
		}
		return angka
	}
}

console.log("SOAL 2")
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log()


// soal 3
function sum(num1, num2=num1, step=1){
	var angka = 0
	if (num1==null) {
		return 0
	}
	else if (num1 > num2) {
		for (var i = num1; i > num2-1; i-=step) {
			angka+=i
		}
		return angka
	}
	else {
		for (var i = num1; i < num2+1; i+=step) {
			angka+=i
		}
		return angka
	}
}

console.log("SOAL 3")
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log()


// soal 4
function dataHandling(dataarray){
	console.log("SOAL 4")
	for (var i = 0; i < dataarray.length; i++) {
		console.log("Nomor ID: " + dataarray[i][0])
		console.log("Nama Lengkap: " + dataarray[i][1])
		console.log("TTL: " + dataarray[i][2])
		console.log("Hobi: " + dataarray[i][3])
		console.log()
	}
	console.log()
}

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 

dataHandling(input)


// SOAL 5
function balikKata(kata){
	var katabalik = ""
	for (var i = kata.length-1; i >= 0; i--) {
		katabalik += kata[i]
	}
	return katabalik
}

console.log("SOAL 5")
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log()

// soal 6
function dataHandling2(array){
	var arraybaru = array

	arraybaru.splice(1,2,array[1] + "Elsharawy", "Provinsi " + array[2])
	arraybaru.splice(4,2, "Pria", "SMA Internasional Metro")
	console.log(arraybaru)

	var tanggal = arraybaru[3].split("/")
	console.log(tanggal)
	var bulan = parseInt(tanggal[1],10)

	console.log(bulann(bulan))

	tanggal.sort(function(a, b){return b-a})

	console.log(tanggal)

	tanggal = tanggal.join("-")

	console.log(tanggal)

	console.log(arraybaru[1].slice(0,15))

}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
console.log("SOAL 6")
dataHandling2(input);


function bulann(bulan){
	switch (bulan){
		case 1:{
			return "Januari"
			break
		}

		case 2:{
			return "Februari"
			break
		}

		case 3:{
			return "Maret"
			break
		}

		case 4:{
			return "April"
			break
		}

		case 5:{
			return "Mei"
			break
		}

		case 6:{
			return "Juni"
			break
		}

		case 7:{
			return "Juli"
			break
		}

		case 8:{
			return "Agustus"
			break
		}

		case 9:{
			return "September"
			break
		}

		case 10:{
			return "Oktober"
			break
		}

		case 11:{
			return "November"
			break
		}

		case 12:{
			return "Desember"
			break
		}

		default : {return "undefined"}
	}

}