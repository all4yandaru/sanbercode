import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, Image, View, TouchableOpacity, FlatList, ScrollView } from 'react-native';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import Icon from 'react-native-vector-icons/MaterialIcons';
import Skill from './components/skillItem'
import data from './skillData.json'
import { Project } from './ProjectScreen';
import { Add } from './AddScreen';

const Tabs = createBottomTabNavigator();

const Kemampuan = () => {
    return(
        <ScrollView style={styles.container}>
            <View style={styles.logo}>
                <Image source={require('./images/logo2.png')}/>
            </View>

            <View style={styles.body}>
                <View style={styles.profile}>
                    <Image style={styles.profileImage} source={require('./images/person.png')}/>
                    <View>
                        <Text style={styles.fontDefault}>Hai,</Text>
                        <Text style={styles.fontDarkMedium}>Liek Allyandaru!</Text>
                    </View>
                </View>

                <View style={styles.skill}>
                    <Text style={styles.fontDarkBig}>SKILL</Text>
                    <View style={{height:4, backgroundColor:'#3EC6FF'}}/>
                </View>

                <View style={styles.categories}>
                    <View style={styles.box}>
                        <Text style={styles.fontDarkNormalBold}>Library / Framework</Text>
                    </View>
                    <View style={styles.box}>
                        <Text style={styles.fontDarkNormalBold}>Bahasa Pemrograman</Text>
                    </View>
                    <View style={styles.box}>
                        <Text style={styles.fontDarkNormalBold}>Teknologi</Text>
                    </View>
                </View>
                

                <FlatList
                data={data.items}
                renderItem={(skill)=><Skill skill={skill.item}/>}
                keyExtractor={(item)=>item.id}
                />
            </View>
            
        </ScrollView>
    )
}

export const SkillScreen = () => {
    return (
        <Tabs.Navigator>
            <Tabs.Screen name="SkillScreen" component={Kemampuan} title="Proyek"/>
            <Tabs.Screen name="Project" component={Project} title="Proyek"/>
            <Tabs.Screen name="Add" component={Add}/>
        </Tabs.Navigator>
        );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  body: {
    paddingHorizontal: 16
  },
  logo: {
      alignItems: 'flex-end',
      marginBottom: 50,
      marginTop: 10,
    },
    profile: {
        flexDirection: 'row',
        marginBottom: 16
    },
    profileImage: {
        marginRight: 5,
    },
    fontDefault: {
        fontSize: 12,
    },
    fontDarkMedium: {
        fontSize: 16,
        color: '#003366'
    },
    fontDarkBig: {
        fontSize: 36,
        color: '#003366'
    },
    fontDarkNormalBold:{
        fontSize: 12,
        color: '#003366',
        fontWeight: 'bold'
    },
    skill: {
        marginBottom: 10
    },
    categories: {
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    box: {
        backgroundColor: '#B4E9FF',
        borderRadius: 16,
        padding: 10,
        marginVertical: 10
    },
});
