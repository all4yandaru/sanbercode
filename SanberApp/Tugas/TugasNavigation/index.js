import React from "react";
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from '@react-navigation/drawer'

import { Login } from './LoginScreen'
import { About } from './AboutScreen'
import { SkillScreen } from './SkillScreen'
import { Project } from './ProjectScreen'
import { Add } from './AddScreen'

const Stack = createStackNavigator();

export default()=>{
  return(
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Login" component={Login}/>
        <Stack.Screen name="About" component={About}/>
      </Stack.Navigator>

      {/*
      <AuthStack.Navigator>
        <AuthStack.Screen name="SignIn" component={SignIn} options={{ title:'Sign In'}}/>
        <AuthStack.Screen name="CreateAccount" component={CreateAccount} options={{ title:'Create Account'}}/>
      </AuthStack.Navigator>*/}
  </NavigationContainer>
  )
}
