import React from "react";
import { StyleSheet, Text, Image, View, TouchableOpacity, FlatList, ScrollView } from 'react-native';

import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

const Stack = createStackNavigator();

const proyek = () => {
    return(
        <View>
            <Text>HALAMAN PROYEK</Text>
        </View>
    )
}
export const Project = ()=>{
  return(
    <Stack.Navigator>
        <Stack.Screen name="project" component={proyek}/>
    </Stack.Navigator>
  )
}
