import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, Image, View, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default class VideoItems extends React.Component {
    render(){
        let skill = this.props.skill;
        return(
            <View style={styles.container}>
                <View style={styles.box}>
                    <View style={styles.box2}>
                        <Icon style={styles.icon} name={skill.iconName} size={80} color={'#003366'}/>
                        <View>
                            <Text style={styles.skillsName}>{skill.skillName}</Text>
                            <Text style={styles.technology}>{skill.categoryName}</Text>
                            <Text style={styles.percentage}>{skill.percentageProgress}</Text>
                        </View>
                        
                    </View>
                    <Image source={require('../images/chevron.png')}/>
                    
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 0,
    },
    box: {
        backgroundColor: '#B4E9FF',
        borderRadius: 16,
        padding: 10,
        marginVertical: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    box2: {
        flexDirection: 'row',
    },
    skillsName: {
        color: '#003366',
        fontSize: 24,
        fontWeight: 'bold'
    },
    technology:{
        color: '#3EC6FF',
        fontSize: 16,
        textAlign: 'left'
    },
    percentage:{
        color: '#FFFFFF',
        fontSize: 48,
        textAlign: 'right',
    },
    icon: {
        marginRight: 5
    },
});