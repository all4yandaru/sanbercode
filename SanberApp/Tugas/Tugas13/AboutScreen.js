import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, Image, View, TouchableOpacity, ScrollView, TextInput, Button, Alert } from 'react-native';


import Icon from 'react-native-vector-icons/MaterialIcons';

export default class App extends React.Component {
    render(){
        //alert(data.kind)
    return (
        <ScrollView style={styles.container}>
            <Text style={styles.titleBig}>Tentang saya</Text>

            <View style={styles.profile}>
                <Icon name="face" size={200} color="#DDDDDD"/>
                <Text style={styles.titleSmall}>Liek Allyandaru</Text>
                <Text style={styles.fontLight}>React Native Developer</Text>
            </View>

            <View style={styles.detail}>
                <View style={styles.box}>
                    <Text style={styles.fontDark}>Portofolio</Text>
                    <View style={{height:0.5, backgroundColor:'#003366', marginVertical: 5, marginBottom: 15}}/>
                    <View style={styles.portofolio}>
                        <View style={{alignItems: 'center'}}>
                            <Image source={require('./image/gitlab.png')}/>
                            <Text style={styles.fontDarkBold}>@all4yandaru</Text>
                        </View>
                        <View style={{alignItems: 'center'}}>
                            <Image source={require('./image/github.png')}/>
                            <Text style={styles.fontDarkBold}>@all4yandaru</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.box}>
                    <Text style={styles.fontDark}>Hubungi Saya</Text>
                    <View style={{height:0.5, backgroundColor:'#003366', marginVertical: 5, marginBottom: 15}}/>
                    <View style={styles.socialMedia}>
                        <View style={styles.account}>
                            <Image style={styles.image} source={require('./image/facebook.png')}/>
                            <Text style={styles.fontDarkBold}>Liek Allyandaru</Text>
                        </View>
                        <View style={styles.account}>
                            <Image style={styles.image} source={require('./image/instagram.png')}/>
                            <Text style={styles.fontDarkBold}>@allyandaru25</Text>
                        </View>
                        <View style={styles.account}>
                            <Image style={styles.image} source={require('./image/twitter.png')}/>
                            <Text style={styles.fontDarkBold}>Tidak ada :)</Text>
                        </View>
                    </View>
                </View>
            </View>
        </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF'
    },
    profile: {
        alignItems: 'center'
    },
    titleSmall: {
        fontSize: 24,
        color: '#003366',
        fontFamily: 'Roboto',
        fontWeight: 'bold'
    },
    titleBig: {
        fontSize: 36,
        height: 60,
        color: '#003366',
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 50,
    },
    fontDark: {
        fontSize: 16,
        color: '#003366',
        fontFamily: 'Roboto',
    },
    fontDarkBold: {
        fontSize: 16,
        color: '#003366',
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        margin: 5,
        textAlignVertical:'center'
    },
    fontLight: {
        fontSize: 16,
        color: '#3EC6FF',
        fontFamily: 'Roboto',
        textAlign: 'center',
        marginTop: 5
    },
    detail: {
        padding: 10,
        marginVertical: 10
    },
    box: {
        backgroundColor: '#EFEFEF',
        borderRadius: 16,
        padding: 10,
        marginVertical: 10
    },
    portofolio: {
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    socialMedia: {
        flexDirection: 'column',
        justifyContent: 'space-around',
    },
    image: {
        marginLeft: 80,
    },
    account:{
        flexDirection: 'row', marginBottom: 20, marginTop:10
    }
})