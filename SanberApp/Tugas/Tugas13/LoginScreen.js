import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, Image, View, TouchableOpacity, ScrollView, TextInput, Button, Alert } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

export default class App extends React.Component {
    render(){
        //alert(data.kind)
    return (
        <ScrollView style={styles.container}>
            <View style={styles.logo}>
                <Image source={require('./image/logo.png')}/>
            </View>

            <View style={styles.appTitle}>
                <Text style={styles.titleSmall}>Login</Text>
            </View>

            <View style={styles.form}>
                <Text style={styles.font}>Username</Text>
                <TextInput
                    style={styles.formInput}
                    placeholder="Type your username!"
                />

                <Text style={styles.font}>Password</Text>
                <TextInput
                    style={styles.formInput}
                    secureTextEntry={true}
                    placeholder="Type your password!"
                />

                <TouchableOpacity
                    style={styles.MasukButtonStyle}
                    activeOpacity = { .5 }
                >
            
                    <Text style={styles.TextStyle}> MASUK </Text>
                </TouchableOpacity>

                <Text style={styles.font2}>Atau</Text>

                <TouchableOpacity
                    style={styles.DaftarButtonStyle}
                    activeOpacity = { .5 }
                >
            
                    <Text style={styles.TextStyle}> DAFTAR ? </Text>
                </TouchableOpacity>
            </View>

            
        </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF'
    },
    appTitle: {
        alignItems: 'center',
        marginBottom: 30
    },
    titleSmall: {
        fontSize: 24,
        color: '#003366',
        fontFamily: 'Roboto'
    },
    titleBig: {
        fontSize: 48,
        color: '#E7D42D',
        fontFamily: 'Roboto'
    },
    form: {
        flex: 1,
        padding: 10,
        marginHorizontal: 20,
    },
    logo: {
        alignItems: 'center',
        marginBottom: 50,
        marginTop: 50
    },
    formInput: {
        borderColor: '#003366',
        borderWidth: 0.5,
        height: 40,
        padding: 5,
        marginBottom: 10
    },
    font: {
        fontSize: 16,
        color: '#003366',
        fontFamily: 'Roboto',
        marginBottom: 5,
        marginHorizontal: 1
    },
    font2: {
        fontSize: 24,
        color: '#3EC6FF',
        fontFamily: 'Roboto',
        marginVertical: 3,
        marginHorizontal: 1,
        textAlign: 'center'
    },
    MasukButtonStyle: {
        marginTop:10,
        paddingTop:15,
        paddingBottom:15,
        marginLeft:30,
        marginRight:30,
        backgroundColor:'#3EC6FF',
        borderRadius:16,
        borderWidth: 1,
        borderColor: '#fff'
    },
    DaftarButtonStyle: {
        marginTop:10,
        paddingTop:15,
        paddingBottom:15,
        marginLeft:30,
        marginRight:30,
        backgroundColor:'#003366',
        borderRadius:16,
        borderWidth: 1,
        borderColor: '#fff'
    },
    TextStyle:{
        color:'#fff',
        textAlign:'center',
    }
})