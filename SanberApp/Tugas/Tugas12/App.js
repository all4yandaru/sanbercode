import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, Image, View, TouchableOpacity, FlatList } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './components/VideoItem'
import data from './data.json'

export default class App extends React.Component {
    render(){
        //alert(data.kind)
    return (
        <View style={styles.container}>
            <View style={{height: 25, backgroundColor: '#E5E5E5'}}/>
            <View style={styles.navBar}>
                <Image source={require('./images/logo.png')} style={{width: 98, height: 22}}/>
                <View style={styles.rightNav}>
                    <TouchableOpacity>
                        <Icon style={styles.navItem} name="search" size={25}/>
                    </TouchableOpacity>
                    
                    <TouchableOpacity>
                        <Icon style={styles.navItem} name="account-circle" size={25}/>
                    </TouchableOpacity>
                </View>
            </View>
            
            <View style={styles.body}>
                <FlatList
                data={data.items}
                renderItem={(video)=><VideoItem video={video.item}/>}
                keyExtractor={(item)=>item.id}
                ItemSeparatorComponent={()=><View style={{height:0.5, backgroundColor:'#E5E5E5'}}/>}
                />
            </View>

            <View style={styles.tabBar}>
                <TouchableOpacity style={styles.tabItem}>
                    <Icon style={styles.item} name="home" size={25}/>
                    <Text style={styles.tabTitle}>Home</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.tabItem}>
                    <Icon style={styles.item} name="whatshot" size={25}/>
                    <Text style={styles.tabTitle}>Trending</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.tabItem}>
                    <Icon style={styles.item} name="subscriptions" size={25}/>
                    <Text style={styles.tabTitle}>Subscriptions</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.tabItem}>
                    <Icon style={styles.item} name="folder" size={25}/>
                    <Text style={styles.tabTitle}>Library</Text>
                </TouchableOpacity>
            </View>
        </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navBar: {
    height: 55,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  rightNav: {
      flexDirection: 'row',
  },
  navItem: {
      marginLeft: 25,
      color: '#3c3c3c',
  },
  body: {
      flex: 1,
  },
  tabBar: {
      backgroundColor: 'white',
      height: 60,
      borderTopWidth: 0.5,
      borderColor: '#E5E5E5',
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center',
  },
  tabItem: {
    justifyContent: 'center',
    color: '#777777',
    alignItems: 'center'
  },
  tabTitle:{
      fontSize: 11,
      color: '#666666',
      paddingTop: 4,
  },
  item: {
    color: '#666666',
  }
});
