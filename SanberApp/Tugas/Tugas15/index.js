import React from "react";
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from '@react-navigation/drawer'

import { SignIn, CreateAccount, Home, Search, Details, Search2, Profile } from './Screen'

const AuthStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const SearchStack = createStackNavigator();

const HomeStackScreen = () =>{
  return(
    <HomeStack.Navigator>
      <HomeStack.Screen name="Home" component={Home}></HomeStack.Screen>
      <HomeStack.Screen name="Details" component={Details}></HomeStack.Screen>
    </HomeStack.Navigator>
  )
  
}

const SearcheStackScreen = () =>{
  return(
    <SearchStack.Navigator>
      <SearchStack.Screen name="Search" component={Search}></SearchStack.Screen>
      <SearchStack.Screen name="Search2" component={Search2}></SearchStack.Screen>
    </SearchStack.Navigator>
  )
  
}

const ProfileStack = createStackNavigator();
const ProfileStackScreen = () =>{
  return(
  <ProfileStack.Navigator>
    <ProfileStack.Screen name="Profile" component={Profile}/>
  </ProfileStack.Navigator>
  )
}

const TabsScreen = () => {
  return(
    <Tabs.Navigator>
      <Tabs.Screen name="Home" component={HomeStackScreen}/>
      <Tabs.Screen name="Search" component={SearcheStackScreen}/>
    </Tabs.Navigator>
    )
}

const Drawer = createDrawerNavigator();

export default()=>{
  return(
    <NavigationContainer>
      <Drawer.Navigator>
        <Drawer.Screen name="Home" component={TabsScreen}/>
        <Drawer.Screen name="Profile" component={ProfileStackScreen}/>
        <Drawer.Screen name="Search" component={SearcheStackScreen}/>
      </Drawer.Navigator>

      {/*
      <AuthStack.Navigator>
        <AuthStack.Screen name="SignIn" component={SignIn} options={{ title:'Sign In'}}/>
        <AuthStack.Screen name="CreateAccount" component={CreateAccount} options={{ title:'Create Account'}}/>
      </AuthStack.Navigator>*/}
  </NavigationContainer>
  )
}
